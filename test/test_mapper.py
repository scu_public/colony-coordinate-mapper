from pathlib import Path
import unittest

import numpy as np

from mapper import ImageProcessor, Mapper


class TestMapper(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestMapper, self).__init__(*args, **kwargs)
        self.work_dir = Path(__file__).parent
        self.ip = ImageProcessor(self.work_dir / "Pickolo_2021-08-02T15-40-55_384-Point_Calibration_Plate.jpg")
        self.ip.process()

    def test_calibration(self):

        mapper = Mapper(self.ip.points)
        mapper.calibrate()

        expected = np.array([1.0030121989221588, -0.2404954645588357])

        self.assertEqual(np.all(np.abs(mapper._optimizer.params - expected) < 1e-8), True)

    def test_transformation(self):

        mapper = Mapper(self.ip.points)
        mapper.calibrate()

        p_expected = np.array([8.887084207427101, 11.609609152910501])
        p = mapper.point_transform(self.ip.points[0, 0], self.ip.points[0, 1], apply_grid_correction=False)
        self.assertEqual(np.all(np.abs(p - np.array(p_expected)) < 1e-8), True)

        # Indx 0 only has one close grid point; we expect the correction to be very precise
        indx = 0
        p_expected = mapper.gt[indx]
        p = mapper.point_transform(self.ip.points[indx, 0], self.ip.points[indx, 1], apply_grid_correction=True)
        self.assertEqual(np.all(np.abs(p - np.array(p_expected)) < 1e-8), True)

        # All others may have from 1 to 4 grid point neighbors; we are happy with a precision of <=0.1mm
        for indx in range(1, mapper.gt.shape[0]):
            p_expected = mapper.gt[indx]
            p = mapper.point_transform(self.ip.points[indx, 0], self.ip.points[indx, 1], apply_grid_correction=True)
            self.assertEqual(np.all(np.abs(p - np.array(p_expected)) < 0.1), True)

        # Test all at once
        p = mapper.list_transform(self.ip.points, apply_grid_correction=True)
        self.assertEqual(np.all(np.abs(p - mapper.gt) < 0.1), True)

    def test_reload_calibration(self):

        # Calibrate
        mapper = Mapper(self.ip.points)
        mapper.calibrate()

        # Save the calibration
        self.assertEqual(mapper.save("calibration.clb"), True)

        # # Reload the calibration
        self.assertEqual(mapper.load("calibration.clb"), True)

        # Test the optimization
        expected = np.array([1.0030121989221588, -0.2404954645588357])
        self.assertEqual(np.all(np.abs(mapper._optimizer.params - expected) < 1e-8), True)

        # Test the mapper with reloaded calibration
        p = mapper.list_transform(self.ip.points, apply_grid_correction=True)
        self.assertEqual(np.all(np.abs(p - mapper.gt) < 0.1), True)

        # Now test creating a Mapper from calibration altogether
        mapper_c = Mapper(calibration_file_name="calibration.clb")

        # Test the new mapper created from the calibration file
        p = mapper_c.list_transform(self.ip.points, apply_grid_correction=True)
        self.assertEqual(np.all(np.abs(p - mapper.gt) < 0.1), True)

        # Now create a Mapper without passing any points and any calibration files
        mapper_e = Mapper()

        # Make sure the mapper knows that it is not calibrated
        self.assertEqual(mapper_e.is_calibrated, False)

        # Transforming points is not allowed
        try:
            _ = mapper_e.list_transform(self.ip.points)
            success = True
        except Exception as _:
            success = False
        self.assertEqual(success, False)

        # Load calibration
        mapper_e.load("calibration.clb")

        # Test the mapping
        p = mapper_e.list_transform(self.ip.points, apply_grid_correction=True)
        self.assertEqual(np.all(np.abs(p - mapper.gt) < 0.1), True)


if __name__ == '__main__':
    unittest.main()
