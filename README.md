# Colony Coordinate Mapper

## Installation

Clone the repository:

```bash
$ git clone https://git.bsse.ethz.ch/scu_public/colony-coordinate-mapper.git ccm
```

Create the enviromnment (this assumes you are using conda):

```bash
$ cd ccm
$ conda env create -f environment.yml
$ conda activate ccm
```
