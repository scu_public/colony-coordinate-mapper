from pathlib import Path
from math import sqrt
from typing import Union

from skimage.io import imread
from skimage.feature import blob_log
from skimage.color import rgb2gray
from skimage.filters import gaussian
from skimage.morphology import opening, disk
import numpy as np
import pickle
from scipy import optimize
from scipy.spatial.distance import cdist


class Mapper:

    def __init__(self,
                 points: Union[None, np.ndarray] = None,
                 origin_y: float = 8.95,
                 origin_x: float = 12.0,
                 step_y: float = 4.5,
                 step_x: float = 4.5,
                 n_rows: int = 16,
                 n_columns: int = 24,
                 *,
                 calibration_file_name: Union[None, str, Path] = None  # Must be specified by name!
                 ):
        """Constructor: takes the set of points to use to calibrate the system and the ground truth parameters.

        @param points Union[None, np.ndarray]
            [Nx2] Numpy array of points in pixel coordinates to be used for calibration; the number
             of pixel positions match the size of fiduciary marks in the ground truth. The positions
             are sorted by row. This is optional. If passed, a call to `calibrate()` will calibrate
             the Mapper.

        @param origin_y float
            y coordinate in mm of the position of the fiduciary mark at the top-left corner of the ground truth.

        @param origin_x float
            x coordinate in mm of the position of the fiduciary mark at the top-left corner of the ground truth.

        @param step_y float
            Distance in mm between two consecutive fiduciary marks of the ground truth in the y direction.

        @param step_x float
            Distance in mm between two consecutive fiduciary marks of the ground truth in the x direction.

        @param n_rows int
            Number of rows of fiduciary points in the ground truth.

        @param n_columns int
            Number of rows of fiduciary points in the ground truth.

                 n_rows: int = 16,
                 n_columns: int = 24,

        @param calibration_file_name Union[None, str, Path]
            Full file name of a calibration file. If passed, the Mapper will be initialized from the file.
            Use this in alternative to passing `points`. This is optional, but if specified it *must* be
            passed by name: e.g. `m = Mapper(calibration_file_name="calibration.clb")`
        """

        # Calibration file version for saving/loading
        self.VERSION = 1

        # Only one of points and calibration_file_name may be not None
        if points is not None and calibration_file_name is not None:
            raise Exception(f"Only one of points and calibration_file_name may be set!")

        # Make sure points is of the expected type and structure
        if points is not None and (type(points) is not np.ndarray or points.shape[1] != 2):
            raise Exception(f"'points must be an [Nx2] Numpy array!")

        # Declare all necessary transformation results
        self.gt_norm = None
        self.gt_cm = None
        self.gt_ext = None
        self.gt_rng = None
        self.gt_rng_norm = None
        self.points_norm = None
        self.points_cm = None
        self.points_ext = None
        self.points_rng = None
        self.points_rng_norm = None
        self.points = None

        # Error correction grid
        self.error_corr_grid = []

        # Build the ground truth

        # Coordinate of the top-left dot in mm
        self.origin_y = origin_y
        self.origin_x = origin_x

        # Step in y and y in mm
        self.step_y = step_y
        self.step_x = step_x

        # Plate "dimensions" (rows, columns)
        self.n_rows = n_rows
        self.n_columns = n_columns

        self.gt = self._create_ground_truth()

        if calibration_file_name is not None:
            # If a valid calibration file is specified, all settings are loaded from file
            if not Path(calibration_file_name).is_file():
                raise Exception(f"The calibration file {calibration_file_name} does not exist!")

            # Try loading the calibration
            if not self.load(calibration_file_name):
                raise Exception(f"The calibration file {calibration_file_name} is not valid!")

        elif points is not None:
            # Points for calibration (make sure to sort them correctly)
            self.points = self._sort_points_as_expected(points.copy())

            # Normalize the ground truth and the points
            self.gt_norm, self.gt_cm, self.gt_ext, self.gt_rng, self.gt_rng_norm = self._normalize(self.gt)
            self.points_norm, self.points_cm, self.points_ext, self.points_rng, self.points_rng_norm = self._normalize(
                self.points)

            # Initialize the optimizer
            self._optimizer = Optimizer()

            # Mark the mapper as not yet calibrated
            self.is_calibrated = False

        else:

            # Mark the mapper as not yet calibrated
            self.is_calibrated = False

    def _apply_correction_from_grid(self, world_points):
        """Apply correction using the error correction grid."""

        # We will correct a copy of the world_points array
        corrected_world_points = world_points.copy()

        for i, point in enumerate(world_points):
            y, x = point

            # Find the (up to) 4 points in the grid that define the cell
            # containing this point

            # Consider the points within a distance <= step size
            i_y, = np.where(np.abs(self.gt[:, 0] - y) < self.step_y)
            i_x, = np.where(np.abs(self.gt[:, 1] - x) < self.step_x)

            # Find the overlapping indices
            common_values = np.intersect1d(i_y, i_x)

            # If no indices were found, we skip the correction of this point
            if len(common_values) == 0:
                continue

            # Extract all relevant grid points and correction vectors
            grid_points = self.gt[common_values]
            error_vectors = self.error_corr_grid[common_values]

            # Calculate all distances from current point to the grid points
            D = cdist(point.reshape((1, 2)), grid_points)

            # Calculate the weights
            W = D.min() / D
            W = (W / W.sum()).reshape((-1, 1))

            # Calculate weighed error vector
            w_error_vectors = error_vectors.copy()
            w_error_vectors = W * w_error_vectors
            corr = np.sum(w_error_vectors, axis=0)

            # Apply the correction
            corrected_world_points[i, :] -= corr

        return corrected_world_points

    def _create_ground_truth(self):
        """Prepare the ground truth."""
        gt_coords = np.zeros((self.n_rows * self.n_columns, 2))
        r = 0
        for x in range(self.n_columns):
            x_c = self.origin_x + x * self.step_x
            for y in range(self.n_rows):
                y_c = self.origin_y + y * self.step_y
                gt_coords[r, 0] = y_c
                gt_coords[r, 1] = x_c
                r += 1
        return gt_coords

    def _normalize(self, data):
        """Normalize the passed set of points to be in the range [0, 1] and centered around its center of mass."""

        # Center of mass
        cm = np.array([data[:, 0].mean(), data[:, 1].mean()])

        # Extrema
        ext = np.array([
            [data[:, 0].min(), data[:, 1].min()],
            [data[:, 0].max(), data[:, 1].max()]
        ])

        # Range
        rng = np.diff(ext, axis=0)[0]

        # Make a copy of the data
        tr_data = data.copy()

        # Normalize the data
        tr_data[:, 0] = (tr_data[:, 0] - ext[0, 0]) / rng[0]
        tr_data[:, 1] = (tr_data[:, 1] - ext[0, 1]) / rng[1]

        # Calculate the center of mass of the normalized data as well (before recentering)
        rng_norm = np.array([tr_data[:, 0].mean(), tr_data[:, 1].mean()])

        # Center the data
        tr_data[:, 0] = tr_data[:, 0] - rng_norm[0]
        tr_data[:, 1] = tr_data[:, 1] - rng_norm[1]

        # Return
        return tr_data, cm, ext, rng, rng_norm

    def _sort_points_as_expected(self, points):
        """Make sure the list of pixel coordinates for calibration is sorted like the ground truth."""

        # Find the median x and y coordinates of all columns and rows
        sorted_points = np.zeros(points.shape)

        step_x = 25
        tmp = points.copy()
        for i in range(self.n_columns):
            # Set threshold
            x_m = tmp[:, 1].min() + step_x

            # Extract all values smaller than x_m
            l = tmp[tmp[:, 1] < x_m, :]

            # Make sure they are exactly n_rows
            assert (l.shape[0] == self.n_rows)

            # Sort them by x coordinate
            sorted_l = l[l[:, 0].argsort()]

            # Copy them into sorted_blobs_log
            sorted_points[i * self.n_rows: (i + 1) * self.n_rows, :] = sorted_l

            # Get rid of the processed entries
            tmp[tmp[:, 1] < x_m, :] = 1e6

        return sorted_points

    def _to_world_coordinates(self, new_points):
        """Transform the passed points (in pixel coordinates) to world coordinates."""

        # First normalize and center the data using the oiginal points extrema
        tr_new_points = np.atleast_2d(new_points.copy())

        tr_new_points[:, 0] = (tr_new_points[:, 0] - self.points_ext[0, 0]) / self.points_rng[0]
        tr_new_points[:, 1] = (tr_new_points[:, 1] - self.points_ext[0, 1]) / self.points_rng[1]
        tr_new_points[:, 0] = tr_new_points[:, 0] - self.points_rng_norm[0]
        tr_new_points[:, 1] = tr_new_points[:, 1] - self.points_rng_norm[1]

        # Then apply the transformation with the passed parameters (from the optimization)
        opt_new_points = self._optimizer.model(tr_new_points, *self._optimizer.params)

        # And now transform back to the world coordinates
        opt_new_points[:, 0] = opt_new_points[:, 0] * self.gt_rng[0] + self.gt_cm[0]
        opt_new_points[:, 1] = opt_new_points[:, 1] * self.gt_rng[1] + self.gt_cm[1]

        return opt_new_points

    def calc_distances(self, source, target):
        """Calculate element-wise distances between source and target."""
        source = np.atleast_2d(source)
        target = np.atleast_2d(target)
        return np.sqrt(np.sum((source - target) ** 2, axis=1)).reshape(-1, 2)

    def calibrate(self):
        """Finds the transformation between the calibration points and the ground truth."""

        # Find the transformation between the normalized versions of the
        # calibration points and the normalized version of the ground truth.
        params = (1.0, 0.0)
        self._optimizer.run(params, self.points_norm, self.gt_norm)

        # Apply the transformation to the normalized points
        opt_new_points = self._optimizer.model(self.points_norm.copy(), *self._optimizer.params)

        # Reset the error correction grid
        self.error_corr_grid = []

        # Transform back to world coordinates
        opt_new_points[:, 0] = opt_new_points[:, 0] * self.gt_rng[0] + self.gt_cm[0]
        opt_new_points[:, 1] = opt_new_points[:, 1] * self.gt_rng[1] + self.gt_cm[1]

        # Calculate error displacement grid
        self.error_corr_grid = opt_new_points - self.gt

        # Set the Mapper is_calibrated property to True
        self.is_calibrated = True

    def load(self, calibration_file_name):
        """Load calibration information from file."""

        # If the file does not exist, return False
        if not Path(calibration_file_name).is_file():
            return False

        # Try reading the file
        with open(calibration_file_name, "rb") as f:
            calibration = pickle.load(f)

        # Check that the version of the file is up-to-date
        if calibration["version"] < self.VERSION:
            print("The configuration file is obsolete.")
            return False

        # Now set all objects
        try:
            self.origin_y = calibration["origin_y"]
            self.origin_x = calibration["origin_x"]
            self.step_y = calibration["step_y"]
            self.step_x = calibration["step_x"]
            self.n_rows = calibration["n_rows"]
            self.n_columns = calibration["n_columns"]
            self.gt = calibration["gt"]
            self.points = calibration["points"]
            self.gt_norm = calibration["gt_norm"]
            self.gt_cm = calibration["gt_cm"]
            self.gt_ext = calibration["gt_ext"]
            self.gt_rng = calibration["gt_rng"]
            self.gt_rng_norm = calibration["gt_rng_norm"]
            self.points_norm = calibration["points_norm"]
            self.points_cm = calibration["points_cm"]
            self.points_ext = calibration["points_ext"]
            self.points_rng = calibration["points_rng"]
            self.points_rng_norm = calibration["points_rng_norm"]
            self.error_corr_grid = calibration["error_corr_grid"]
            self._optimizer = calibration["optimizer"]

            # Set the is_calibrated flag
            self.is_calibrated = True

            # Return success
            return True

        except Exception as _:

            # Set the is_calibrated to False
            self.is_calibrated = True

            # Return failure
            return False

    def save(self, calibration_file_name):
        """Save calibration information to file."""

        if not self.is_calibrated:
            raise Exception("The Mapper has not been calibrated yet!")

        calibration = {
            "version": self.VERSION,
            "origin_y": self.origin_y,
            "origin_x": self.origin_x,
            "step_y": self.step_y,
            "step_x": self.step_x,
            "n_rows": self.n_rows,
            "n_columns": self.n_columns,
            "gt": self.gt,
            "points": self.points,
            "gt_norm": self.gt_norm,
            "gt_cm": self.gt_cm,
            "gt_ext": self.gt_ext,
            "gt_rng": self.gt_rng,
            "gt_rng_norm": self.gt_rng_norm,
            "points_norm": self.points_norm,
            "points_cm": self.points_cm,
            "points_ext": self.points_ext,
            "points_rng": self.points_rng,
            "points_rng_norm": self.points_rng_norm,
            "error_corr_grid": self.error_corr_grid,
            "optimizer": self._optimizer
        }

        try:
            with open(calibration_file_name, "wb") as f:
                pickle.dump(calibration, f)
            return True

        except Exception as _:
            print(f"Could not save calibration to {calibration_file_name}.")
            return False

    def list_transform(self, new_points, apply_grid_correction=True):
        """Transform new points using the calibration obtained by `calibrate()`."""

        if not self.is_calibrated:
            raise Exception("The Mapper is not calibrated yet!")

        # Apply the calibration
        w_coords = self._to_world_coordinates(new_points)

        # Apply the error correction
        if not apply_grid_correction:
            return w_coords

        f_coords = self._apply_correction_from_grid(w_coords)

        return f_coords

    def point_transform(self, y, x, apply_grid_correction=True):
        """Transform specific point using the calibration obtained by `calibrate()`."""
        points = np.array([y, x]).reshape((1, 2))
        tr_points = self.list_transform(points, apply_grid_correction)
        tr_y, tr_x = tr_points[0]
        return tr_y, tr_x


class Optimizer:

    def __init__(self):
        self.params = []

    @staticmethod
    def calc_sse(y, y_hat):
        """Calculate the Sum of Squared Errors between prediction y_hat and data y."""
        sse = np.sum(np.power(y_hat - y, 2))
        return sse

    @staticmethod
    def model(x, s, angle):
        """Apply the transformation to the data."""
        y = x.copy()

        y[:, 0] = y[:, 0] * s
        y[:, 1] = y[:, 1] * s

        alpha = np.radians(angle)
        R = np.array([[np.cos(alpha), -np.sin(alpha)], [np.sin(alpha), np.cos(alpha)]])
        y = y.dot(R)

        return y

    @staticmethod
    def objf(params, x, y):
        """Objective function to minimize."""
        s = params[0]
        a = params[1]
        y_hat = Optimizer.model(x, s, a)
        sse = Optimizer.calc_sse(y_hat, y)
        return sse

    def run(self, params, x, y):
        """Run the optimization. Parameters to optimize are scaling and rotation angle."""
        self.params = optimize.fmin(Optimizer.objf, params, args=(x, y))


class ImageProcessor:

    def __init__(self, image_file_name):
        if not Path(image_file_name).is_file():
            raise Exception(f"File {image_file_name} not found!")
        self.image_file_name = image_file_name
        self.raw_image = imread(self.image_file_name)
        self.image = None
        self.points = []
        self.radii = []
        self._prepare()

        # Constants
        self.n_columns = 24
        self.n_rows = 16

    def _invert(self, I):
        """Invert image."""
        return (I - I.max()) / (I.min() - I.max())

    def _prepare(self):

        # Invert 
        image_gray = rgb2gray(self.raw_image)
        image_gray = self._invert(image_gray)
        image_gray = gaussian(image_gray, sigma=1)

        # Find the large areas
        selem = disk(7)
        background = opening(image_gray, selem)

        # Subtract the background
        self.image = image_gray - background

    def process(self):
        """Extract the positions of the dots."""

        # Run the `blob_log` function: the threshold is an important parameter to tweak
        blobs_log = blob_log(self.image, min_sigma=5, max_sigma=5, num_sigma=1, threshold=0.01, exclude_border=100)

        # We compute the radii from the 3rd columns (indexing is 0-based)
        blobs_log[:, 2] = blobs_log[:, 2] * sqrt(2)

        # Take 15 dots with the lowest x coordinate, 15 dots with the highest x coordinate,
        # 25 dots with the lowest y coordinate, and 25 dots with the highest y coordinate to estimate
        # the outline of the gird and get rid of outliers
        x = blobs_log[:, 1].copy()
        y = blobs_log[:, 0].copy()
        x.sort()
        y.sort()
        slack = 10
        x0 = np.median(x[:15]) - slack
        x = np.median(x[-15:]) + slack
        y0 = np.median(y[:25]) - slack
        y = np.median(y[-25:]) + slack
        l = np.where(
            (blobs_log[:, 1] < x0) | (blobs_log[:, 1] > x) |
            (blobs_log[:, 0] < y0) | (blobs_log[:, 0] > y)
        )
        filtered_blobs_log = blobs_log.copy()
        filtered_blobs_log = np.delete(filtered_blobs_log, l, axis=0)

        # Find the median x and y coordinates of all columns and rows
        sorted_blobs_log = np.zeros(blobs_log.shape)

        step_x = 25
        tmp = blobs_log.copy()
        for i in range(self.n_columns):
            # Set threshold
            x_m = tmp[:, 1].min() + step_x

            # Extract all values smaller than x_m
            l = tmp[tmp[:, 1] < x_m, :]

            # Make sure they are exactly n_rows
            assert (l.shape[0] == self.n_rows)

            # Sort them by x coordinate
            sorted_l = l[l[:, 0].argsort()]

            # Copy them into sorted_blobs_log
            sorted_blobs_log[i * self.n_rows: (i + 1) * self.n_rows, :] = sorted_l

            # Get rid of the processed entries
            tmp[tmp[:, 1] < x_m, :] = 1e6

        # Assign the results
        self.radii = sorted_blobs_log[:, 2]
        sorted_blobs_log_no_radii = np.delete(sorted_blobs_log, 2, axis=1)
        self.points = sorted_blobs_log_no_radii

    def save(self, out_file_name):
        """Save the extracted points to file."""

        # Save coordinates to disk
        with open(out_file_name, 'wb') as f:
            pickle.dump([self.points, self.radii], f)
